$(document).ready(function() {
    contactReport_datatableInit();

    contactReport_chartInit();
});

function contactReport_exportToExcel() {
    XlsxPopulate.fromBlankAsync()
        .then(workbook => {
            let data = contactReport_search();
            let columns = [];
            let rows = [];
            $.when.apply($, data["columns"].map(function(columnDef) {
                columns.push(columnDef['title']);
            })).then(function() {
                let sheet_teams = workbook.sheet(0).name("Teams");
                sheet_teams.cell("A2").value(columns);

                $.when.apply($, data["rows"].map(function(rowDef) {
                    let row = [];
                    $.each(rowDef, function (k, v) {
                        row.push(v);
                    });
                    rows.push(row);
                })).then(function() {
                    sheet_teams.cell("A3").value(rows);

                    workbook.outputAsync("base64")
                        .then(function (base64) {
                            location.href = "data:" + XlsxPopulate.MIME_TYPE + ";base64," + base64;
                        });
                });
            });
        });
}

function contactReport_datatableInit() {
    let $table = $('#contactReportList');
    let data = contactReport_search();

    let datatable = $table.DataTable( {
        data: data["rows"],
        processing: true,
        responsive: true,
        paging: false,
        bInfo: false,
        searching: false,
        columns: data["columns"],
        order: [[ 0, 'asc' ]]
    } );

}

function contactReport_search() {
    let result = {};
    let rows = [];
    let columns = [];
    let fechas = [];
    columns.push({data: "name", title: "Contacto"})
    let teams = collection.find({}, {
        $orderBy: {
            fecha: 1
        },
        contacto: 1,
        _id: 0,
        fecha: 1,
        $groupBy: {
            "contacto": 1
        }
    });
    $.each(teams, function (k, v) {
        if(k !== "$cursor" && k !== "__fdbOp") {
            let row = {};
            row["name"] = k;
            $.each(v, function (key, value) {
                let fecha = $.format.date(Date.parse(new Date(value["fecha"])), "yyyyMM");
                row[fecha] = (typeof row[fecha]==="undefined"?0:row[fecha]) + 1;
                if ($.inArray(fecha, fechas) === -1) {
                    fechas.push(fecha);
                    columns.push({data: fecha, defaultContent: 0, title: $.format.date(Date.parse(new Date(value["fecha"])), "MMMM yyyy")});
                }
            });
            rows.push(row);
        }
    });
    result["columns"] = columns;
    result["rows"] = rows;

    return result;
}

function contactReport_chartInit() {
    let tableDef = $('#contactReportList').dataTable();
    let tableData = $('#contactReportList').DataTable();
    let columnsKeys = [];
    let columnsLabels = [];
    let chartData = [];
    $.each(tableDef.fnSettings().aoColumns, function (index, column) {
        if(column.mData !== "name") {
            let chartItem = {};
            chartItem["y"] = column.sTitle;
            tableData.rows().every(function (rowIdx, tableLoop, rowLoop) {
                let data = this.data();
                if ($.inArray(rowIdx, columnsKeys) === -1) { columnsKeys.push(rowIdx) };
                if ($.inArray(data["name"], columnsLabels) === -1) { columnsLabels.push(data["name"]) };
                chartItem[rowIdx] = typeof data[column.mData] === "undefined" ? 0 : data[column.mData];
            });
            chartData.push(chartItem);
        }
    });

    Morris.Line({
        resize: true,
        element: 'contactReportChart',
        data: chartData,
        xkey: 'y',
        ykeys: columnsKeys,
        labels: columnsLabels,
        parseTime: false,
        hideHover: true,
        barColors: ['#0088cc', '#2baab1', '#ff9900', '#cc0000', '#336600', '#cccc00']
    });
}

