const electron = require('electron');
window.$ = window.jQuery = require('jquery');
require( 'datatables.net-select' )( window, $ );
const XlsxPopulate = require('xlsx-populate');

let collection = electron.remote.getGlobal('collection');

$(document).ready(function() {
    $('#bodyContent').load("incident_list.html", function() {
        $(document).ready(function() {
            loadScript("../js/incident_list.js");
        });
    });
});

function openPage(page) {
    $('#bodyContent').load(page, function() {
        $(document).ready(function() {
            loadScript("../js/" + (page.split(".")[0]) + ".js");
        });
    });
}

function loadScript(scriptFile) {

    $.getScript( "../assets/javascripts/theme.js" ).done(function( script, textStatus ) {
    }).fail(function( jqxhr, settings, exception ) {
        console.log(exception);
    });

    $.getScript( "../assets/javascripts/theme.init.js" ).done(function( script, textStatus ) {
    }).fail(function( jqxhr, settings, exception ) {
        console.log(exception);
    });

    $.getScript( scriptFile ).done(function( script, textStatus ) {
    }).fail(function( jqxhr, settings, exception ) {
        console.log(exception);
    });
}