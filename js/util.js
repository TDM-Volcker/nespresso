function showProcessing(show) {
    if(show) {
        $('#body_processing').show();
        $('#body_processing').css('opacity', '.5');
    } else {
        $('#body_processing').animate({opacity: 0}, 400, function(){
            $('#body_processing').hide();
        });
    }
}

function formatBoolean(data, type, row) {
    return (data === true) ? "Si" : "No"
}

function formatDate(data, type, row) {
    if(data === null || data === "") {
        return "";
    }
    return $.format.date(Date.parse(new Date(data)), "dd/MM/yyyy");
}

function GetURLParameter(sParam) {
    let response = "";
    let sPageURL = window.location.search.substring(1);
    let sURLVariables = sPageURL.split('&');
    for (let i = 0; i < sURLVariables.length; i++) {
        let sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            response = sParameterName[1];
        }
    }
    return response;
}

const daysOfWeekSorter = {
    "sunday": 0,
    "monday": 1,
    "tuesday": 2,
    "wednesday": 3,
    "thursday": 4,
    "friday": 5,
    "saturday": 6
};