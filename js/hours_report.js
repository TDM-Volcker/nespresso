$(document).ready(function() {
    hoursReport_datatableInit();

    hoursReport_chartInit();
});

function hoursReport_exportToExcel() {
    XlsxPopulate.fromBlankAsync()
        .then(workbook => {
            let data = contactReport_search();
            let columns = [];
            let rows = [];
            $.when.apply($, data["columns"].map(function(columnDef) {
                columns.push(columnDef['title']);
            })).then(function() {
                let sheet_teams = workbook.sheet(0).name("Teams");
                sheet_teams.cell("A2").value(columns);

                $.when.apply($, data["rows"].map(function(rowDef) {
                    let row = [];
                    $.each(rowDef, function (k, v) {
                        row.push(v);
                    });
                    rows.push(row);
                })).then(function() {
                    sheet_teams.cell("A3").value(rows);

                    workbook.outputAsync("base64")
                        .then(function (base64) {
                            location.href = "data:" + XlsxPopulate.MIME_TYPE + ";base64," + base64;
                        });
                });
            });
        });
}

function hoursReport_datatableInit() {
    let $table = $('#hoursReportList');
    let data = hoursReport_search();

    let datatable = $table.DataTable( {
        data: data["rows"],
        processing: true,
        responsive: true,
        paging: false,
        bInfo: false,
        searching: false,
        columns: data["columns"],
        order: [[ 0, 'asc' ]]
    } );

}

function hoursReport_search() {
    let result = {};
    let rows = [];
    let columns = [];
    let fechas = [];
    columns.push({data: "name", title: "Hora"})
    let hours = collection.find({}, {
        $orderBy: {
            fecha: 1
        },
        hora: 1,
        _id: 0,
        fecha: 1,
        $groupBy: {
            "hora": 1
        }
    });
    $.each(hours, function (k, v) {
        if(k !== "$cursor" && k !== "__fdbOp") {
            let row = {};
            row["name"] = ("0"+k).slice(-5);
            $.each(v, function (key, value) {
                let fecha = $.format.date(Date.parse(new Date(value["fecha"])), "ddd");
                row[fecha] = (typeof row[fecha]==="undefined"?0:row[fecha]) + 1;
                if ($.inArray(fecha, fechas) === -1) {
                    fechas.push(fecha);
                    columns.push({data: fecha, defaultContent: 0, title: fecha});
                }
            });
            rows.push(row);
        }
    });
    columns.sort(function sortByDay(a, b) {
        let day1 = a.title.toLowerCase();
        let day2 = b.title.toLowerCase();
        return daysOfWeekSorter[day1] > daysOfWeekSorter[day2];
    });
    result["columns"] = columns;
    result["rows"] = rows;

    return result;
}

function hoursReport_chartInit() {
    let tableDef = $('#hoursReportList').dataTable();
    let tableData = $('#hoursReportList').DataTable();
    let columnsKeys = [];
    let columnsLabels = [];
    let chartData = [];
    tableData.rows().every(function (rowIdx, tableLoop, rowLoop) {
        let data = this.data();
        let chartItem = {};
        chartItem["y"] = data["name"];
        $.each(tableDef.fnSettings().aoColumns, function (index, column) {
            if(column.mData !== "name") {
                if ($.inArray(index, columnsKeys) === -1) {
                    columnsKeys.push(index);
                }
                if ($.inArray(column.sTitle, columnsLabels) === -1) {
                    columnsLabels.push(column.sTitle);
                }
                chartItem[index] = typeof data[column.mData] === "undefined" ? 0 : data[column.mData];
            }
        });
        chartData.push(chartItem);
    });

    Morris.Bar({
        resize: true,
        element: 'hoursReportChart',
        data: chartData,
        xkey: 'y',
        ykeys: columnsKeys,
        labels: columnsLabels,
        parseTime: false,
        hideHover: false,
        barColors: ['#0088cc', '#2baab1', '#ff9900', '#cc0000', '#336600', '#cccc00']
    });
}

