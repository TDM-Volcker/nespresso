$(document).ready(function() {
    $("#deleteBtn").prop('disabled', true);
    $("#editBtn").prop('disabled', true);

    incidentList_datatableInit();

    $('.modal-basic').magnificPopup({
        type: 'inline',
        preloader: false,
        modal: true
    });

    $('.new-modal').magnificPopup({
        type: 'ajax',
        modal: true,
        callbacks: {
            parseAjax: function(mfpResponse) {
            },
            ajaxContentAdded: function() {
                $.getScript( "../assets/javascripts/theme.init.js");
                $('#_id').val(null);
            }
        }
    });

    $('.edit-modal').magnificPopup({
        type: 'ajax',
        modal: true,
        callbacks: {
            parseAjax: function(mfpResponse) {
            },
            ajaxContentAdded: function() {
                $.getScript( "../assets/javascripts/theme.init.js");

                $( "#fecha" ).datepicker({
                    format: "dd/mm/yyyy",
                    language: "es",
                    todayBtn: "linked",
                    todayHighlight: "true",
                    weekStart: "0",
                    orientation: "top"
                });

                let table = $('#incidentList').DataTable();
                let selectedRow = table.row('.selected').data();
                if(typeof selectedRow !== 'undefined') {
                    $.each(selectedRow, function(k, v) {
                        if($('#'+k).attr("name") === "fecha") {
                            if(v !== ""){
                                $('#'+k).datepicker("setDate", new Date(Date.parse(new Date(v))) );
                            }
                        } else if($('#'+k).attr("type") === "checkbox") {
                            $('#'+k).prop('checked', v);
                        } else {
                            $('#'+k).val(v);
                        }
                    });
                } else {
                    new PNotify({
                        title: 'Warning!',
                        text: 'No incident selected',
                        type: 'warning'
                    });
                }
            }
        }
    });

    $(document).on('click', '.modal-dismiss', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        $.magnificPopup.close();
    });

    /*
    Modal Confirm
    */
    $(document).on('click', '.modal-confirm', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        incidentList_saveIncident();
    });

    $(document).on('click', '.dismissBtn', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        $.magnificPopup.close();
    });

    /*
    Modal Confirm
    */
    $(document).on('click', '.confirmationBtn', function (e) {
        e.stopImmediatePropagation();
        e.preventDefault();
        deleteIncident();
        $.magnificPopup.close();
    });

    $( "#filtro_desde" ).datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        todayBtn: "linked",
        todayHighlight: "true",
        weekStart: "0",
        orientation: "top",
        clearBtn: "true"
    }).on('changeDate', function (selected) {
        let table = $('#incidentList').DataTable();
        table.clear();
        table.rows.add(search());
        table.draw();
    });

    $( "#filtro_hasta" ).datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        todayBtn: "linked",
        todayHighlight: "true",
        weekStart: "0",
        orientation: "top",
        clearBtn: "true"
    }).on('changeDate', function (selected) {
        let table = $('#incidentList').DataTable();
        table.clear();
        table.rows.add(search());
        table.draw();
    });

});

function incidentList_saveIncident() {
    let items = [];
    let item = {
        equipo: $('#equipo').val(),
        crc: $('#crc').prop('checked'),
        fecha: $('#fecha').val()!==""?$('#fecha').datepicker( "getDate" ).getTime():"",
        hora: $('#hora').val(),
        apex: $('#apex').prop('checked'),
        contacto: $('#contacto').val(),
        ucs: $('#ucs').val(),
        team_leader: $('#team_leader').val(),
        back_office: $('#back_office').val(),
        incidencia: $('#incidencia').val(),
        ncm: $('#ncm').val(),
        problema: $('#problema').val(),
        problema_resuelto: $('#problema_resuelto').prop('checked'),
        reenviado: $('#reenviado').prop('checked'),
        share_club: $('#share_club').prop('checked'),
        fcr: $('#fcr').val(),
        modification_date: new Date()
    };

    if($('#_id').val() !== null && $('#_id').val() !== "") {
        item['_id'] = $('#_id').val();
        collection.update({"_id": item['_id']}, {$replace: item});
    } else {
        item['creation_date'] = new Date();
        let result = collection.insert(item);
        items.push(result['inserted'][0]);
    }

    collection.save(function (err) {
        if (!err) {
            let table = $('#incidentList').DataTable();
            table.clear();
            table.rows.add(search());
            table.draw();
        } else {
            console.log("Error al guardar", err);
        }
        $.magnificPopup.close();

        new PNotify({
            title: 'Success!',
            text: 'Incident registered.',
            type: 'success'
        });
    });
};

function exportToExcel() {
    XlsxPopulate.fromFileAsync("./templates/report_2nd_line.xlsx")
        .then(workbook => {
            let data = search();
            let listToExport = [];
            $.when.apply($, data.map(function(incident) {
                let reg = [];
                reg.push(incident['ucs']);
                reg.push(incident['equipo']);
                reg.push(incident['crc']?"SI":"NO");
                reg.push(incident['apex']?"SI":"NO");
                reg.push($.format.date(Date.parse(new Date(incident['fecha'])), "dd/MM/yyyy"));
                reg.push(incident['hora']);
                reg.push(incident['contacto']);
                reg.push(incident['team_leader']);
                reg.push(incident['back_office']);
                reg.push(incident['incidencia']);
                reg.push(incident['ncm']);
                reg.push(incident['problema']);
                reg.push(incident['share_club']?"SI":"NO");
                reg.push(incident['problema_resuelto']?"SI":"NO");
                reg.push(incident['reenviado']?"SI":"NO");
                reg.push(incident['fcr']);

                listToExport.push(reg);
            })).then(function() {
                workbook.sheet("2nd Line").cell("A3").value(listToExport);

                workbook.outputAsync("base64")
                    .then(function (base64) {
                        location.href = "data:" + XlsxPopulate.MIME_TYPE + ";base64," + base64;
                    });
            });
        });
};

function incidentList_datatableInit() {
    let $table = $('#incidentList');

    let datatable = $table.DataTable( {
        data: search(),
        processing: true,
        responsive: true,
        select: {
            style: 'single'
        },
        columns: [
            { data: "equipo", title: "Equipo" },
            { data: "fecha", title: "Fecha", render: formatDate},
            { data: "hora", title: "Hora" },
            { data: "crc", title: "CRC", render: formatBoolean},
            { data: "apex", title: "Apex", render: formatBoolean},
            { data: "incidencia", title: "Incidencia" },
            { data: "problema_resuelto", title: "Resuelto", render: formatBoolean},
            { data: "reenviado", title: "Reenviado", render: formatBoolean},
            { data: "team_leader", title: "TL" },
            { data: "fcr", title: "FCR" },
            { data: "contacto", title: "Contacto" },
            { data: "_id", title: "ID", visible: false }
        ],
        order: [[ 1, 'desc' ]]
    } );

    datatable.on( 'deselect', function ( e, dt, type, indexes ) {
        $("#deleteBtn").prop('disabled', true);
        $("#editBtn").prop('disabled', true);
    });

    datatable.on( 'select', function ( e, dt, type, indexes ) {
        $("#deleteBtn").prop('disabled', false);
        $("#editBtn").prop('disabled', false);
    } );
};

function deleteIncident() {
    showProcessing(true);
    let table = $('#incidentList').DataTable();
    let selectedRow = table.row('.selected').data();
    if(typeof selectedRow !== 'undefined') {
        collection.remove({_id: selectedRow._id});
        collection.save(function (err) {
            if (!err) {
                table.row('.selected').remove().draw( false );
                $("#deleteBtn").prop('disabled', true);
                $("#editBtn").prop('disabled', true);

                new PNotify({
                    title: 'Success!',
                    text: 'Incident deleted.',
                    type: 'success'
                });
            } else {
                console.log("Error al guardar", err);
            }
            showProcessing(false);
        });
    } else {
        new PNotify({
            title: 'Warning!',
            text: 'No incident selected',
            type: 'warning'
        });
        showProcessing(false);
    }

}

function search() {
    let date_filter = {};
    if($('#filtro_desde').val() !== "" || $('#filtro_hasta').val() !== "") {
        date_filter["fecha"] = {};
        if($('#filtro_desde').val() !== "") {
            date_filter["fecha"]["$gte"] = $('#filtro_desde').datepicker("getDate").getTime();
        }
        if($('#filtro_hasta').val() !== "") {
            date_filter["fecha"]["$lte"] = $('#filtro_hasta').datepicker("getDate").getTime();
        }
    }
    return collection.find(date_filter, {$orderBy: {fecha: 1}});
}


