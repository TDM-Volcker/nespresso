const {app, BrowserWindow} = require('electron');
const ForerunnerDB = require("forerunnerdb");
const isDev = require('electron-is-dev');


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let collection;

function init () {
    if (isDev) {
        require('electron-reload')(__dirname);
    }
    createDataBase();
    createWindow();
}

function createDataBase() {

    let fdb = new ForerunnerDB();
    let db = fdb.db("nespresso");
    db.persist.dataDir(app.getPath('userData') + "\\data");
    collection = db.collection("incidencias").deferredCalls(false);

    collection.load(function (err) {
        if (!err) {
            console.log("Database loaded");
        } else {
            console.log("Error loading database", err);
        }
    });

    global.collection = collection;
}

function createWindow () {
    // Crea la ventana del navegador.
    win = new BrowserWindow({
        width: 1024,
        height: 600,
        center: true,
        autoHideMenuBar: true,
        title: "Nespresso - Incident Registration",
        webPreferences: {
            devTools: isDev?true:false
        }
    });

    // y carga el archivo index.html de la aplicación.
    win.loadFile('pages/index.html');

    // Open the DevTools.
    if (isDev) {
        win.webContents.openDevTools();
    }

    // Emitido cuando la ventana es cerrada.
    win.on('closed', () => {
        // Desreferencia el objeto ventana, usualmente tu guardarias ventanas
        // en un arreglo si tu aplicación soporta multi ventanas, este es el momento
        // cuando tu deberías borrar el elemento correspiente.
        win = null
    })

    global.windows = win;
}

// Este método será llamado cuando Electron haya terminado
// la inicialización y esté listo para crear ventanas del navegador.
// Algunas APIs pueden solamente ser usadas despues de que este evento ocurra.
app.on('ready', init);

// Salir cuando todas las ventanas estén cerradas.
app.on('window-all-closed', () => {
    app.quit()
});

app.on('activate', () => {
    // En macOS es común volver a crear una ventana en la aplicación cuando el
    // icono del dock es clickeado y no hay otras ventanas abieras.
    if (win === null) {
        createWindow()
    }
});

// En este archivo tu puedes incluir el resto del código del proceso principal de
// tu aplicación. Tu también puedes ponerlos en archivos separados y requerirlos aquí.